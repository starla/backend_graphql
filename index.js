const graphqlHttp = require("express-graphql");
const express = require("express");
const bodyParser = require("body-parser");
const schema = require("./schema");
const { PORT = 2002, NODE_ENV = "dev" } = process.env;

const app = express();
var cors = require("cors");
const voyagerMiddleware = require("graphql-voyager/middleware").express;
app.use(cors());
app.use(
  "/_graphql",
  graphqlHttp({
    schema,
    graphiql: true
  })
);

app.use("/voyager", voyagerMiddleware({ endpointUrl: "/_graphql" }));
app.listen(PORT, () =>
  console.log(`Listen on port http://localhost:${PORT}/_graphql`)
);
