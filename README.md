# Middleware para la constuccion de nodos que consultan API de datos demograficos para la constuccion de grafica de IPC

## Instalación de dependencias

Entrar a la raiz del proyecto de `backend_graphql` e instalar las dependencias con la siguiente instrucción

```
$npm install
```

una ve instalado las dependencias, ejecutar la siguiente lineas para iniciar el servidor de Graphql de manera local

```
$npm rum dev
```

el servidor inciara en el puerto 2002 y para acceder a la consola de Consultas de Graphql será a travez de la siguiente dirección

`http://localhost:2002/_graphql`

para ver el diagrama "Entidad relacion entre los eschemas construidos con graphql será a traves de la siguiente dirección"

`http://localhost:2002/voyager`
