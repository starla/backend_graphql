const { GraphQLObjectType } = require("graphql");

module.exports = new GraphQLObjectType({
  name: "Query",
  description: "Metodos de consulta para Test de GBM",
  fields: {
    viewer: {
      type: require("./viewer"),
      resolve: require("../resolvers/query.viewer")
    }
  }
});
