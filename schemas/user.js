const { GraphQLString, GraphQLObjectType } = require("graphql");

module.exports = new GraphQLObjectType({
  name: "User",
  description: "Schema de usuario",
  fields: {
    nickname: {
      type: GraphQLString
    },
    mail: {
      type: GraphQLString
    },
    password: {
      type: GraphQLString
    },
    name: {
      type: GraphQLString
    }
  }
});
