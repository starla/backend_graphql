const { GraphQLObjectType, GraphQLString } = require("graphql");

module.exports = new GraphQLObjectType({
  name: "Mutation",
  description: "Mutaciones de registros",
  fields: () => ({
    users: {
      type: require("./user"),
      description: "Envia datos de registro nuestra base de datos",
      args: {
        nickname: {
          type: GraphQLString
        },
        mail: {
          type: GraphQLString
        },
        password: {
          type: GraphQLString
        },
        name: {
          type: GraphQLString
        }
      },
      resolve: require("../resolvers/add.user")
    }
  })
});
