const {
  GraphQLString,
  GraphQLObjectType,
  GraphQLFloat,
  GraphQLInt
} = require("graphql");

module.exports = new GraphQLObjectType({
  name: "DataIPC",
  description: "Argumentos para el filtrado de datos",
  fields: {
    Fecha: {
      type: GraphQLString
    },
    Precio: {
      type: GraphQLFloat
    },
    Porcentaje: {
      type: GraphQLFloat
    },
    Volumen: {
      type: GraphQLInt
    }
  }
});
