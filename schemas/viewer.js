const { GraphQLList, GraphQLObjectType } = require("graphql");
const fetch = require("node-fetch");

module.exports = new GraphQLObjectType({
  name: "Viewer",
  fields: () => ({
    dataIPC: {
      type: new GraphQLList(require("./dataIPC")),
      resolve: () =>
        fetch(`https://www.gbm.com.mx/Mercados/ObtenerDatosGrafico?empresa=IPC`)
          .then(response => response.json())
          .then(response => response.resultObj)
    }
  })
});
